package ru.t1.skasabov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.skasabov.tm.dto.CustomUser;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;

@Controller
public final class ProjectController {

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @GetMapping("/project/create")
    public String create(@AuthenticationPrincipal @NotNull final CustomUser user) {
        @NotNull final ProjectDto project = new ProjectDto("New Project " + System.currentTimeMillis());
        project.setUserId(user.getUserId());
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") @NotNull final String id,
            @AuthenticationPrincipal @NotNull final CustomUser user
    ) {
        taskRepository.deleteByUserIdAndProjectId(user.getUserId(), id);
        projectRepository.deleteByUserIdAndId(user.getUserId(), id);
        return "redirect:/projects";
    }

    @NotNull
    @PostMapping("/project/edit/{project.id}")
    public String edit(
            @ModelAttribute("project") @NotNull final ProjectDto project,
            @AuthenticationPrincipal @NotNull final CustomUser user
    ) {
        project.setUserId(user.getUserId());
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id,
            @AuthenticationPrincipal @NotNull final CustomUser user
    ) {
        @Nullable final ProjectDto project = projectRepository.findByUserIdAndId(user.getUserId(), id);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    @NotNull
    private Status[] getStatuses() {
        return Status.values();
    }

}
