package ru.t1.skasabov.tm.integration.rest;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.skasabov.tm.client.rest.AuthRestEndpointClient;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.marker.IntegrationCategory;

public class AuthRestEndpointClientTest {

    @NotNull
    private final AuthRestEndpointClient client = AuthRestEndpointClient.client();

    private boolean isLogout = true;

    @Before
    public void initTest() {
        Assert.assertTrue(client.login("test", "test").isSuccess());
    }

    @After
    public void clean() {
        if (isLogout) {
            client.logout();
        }
        isLogout = true;
    }

    @Test
    @Category(IntegrationCategory.class)
    public void loginTest() {
        client.logout();
        Assert.assertTrue(client.login("test", "test").isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void emptyLoginTest() {
        isLogout = false;
        client.logout();
        Assert.assertFalse(client.login("", "test").isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void loginEmptyPasswordTest() {
        isLogout = false;
        client.logout();
        Assert.assertFalse(client.login("test", "").isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void incorrectLoginTest() {
        isLogout = false;
        client.logout();
        Assert.assertFalse(client.login("admin", "test").isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void loginIncorrectPasswordTest() {
        isLogout = false;
        client.logout();
        Assert.assertFalse(client.login("test", "admin").isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void profileTest() {
        @Nullable final UserDto user = client.profile();
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void profileNoAuthTest() {
        isLogout = false;
        client.logout();
        client.profile();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void logoutTest() {
        isLogout = false;
        Assert.assertFalse(client.logout().isSuccess());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void logoutNoAuthTest() {
        isLogout = false;
        client.logout();
        client.logout();
    }

}
