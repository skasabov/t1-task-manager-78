FROM maven:3.6.3-jdk-8 as maven
RUN mkdir /src
COPY . /src
WORKDIR /src
RUN mvn clean install

FROM eclipse-temurin:8-jdk as docker
COPY --from=maven /src/target/tm-web.war /opt/tm-web.war
WORKDIR /opt
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "tm-web.war"]